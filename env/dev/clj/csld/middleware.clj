(ns csld.middleware
  (:require
   [ring.middleware.content-type :refer [wrap-content-type]]
   [ring.middleware.params :refer [wrap-params]]
   [prone.middleware :refer [wrap-exceptions]]
   [reitit.ring.middleware.muuntaja :as muuntaja]
   [ring.middleware.reload :refer [wrap-reload]]
   [ring.middleware.defaults :refer [site-defaults wrap-defaults]]))

(def middleware
  #_[muuntaja/format-negotiate-middleware
     muuntaja/format-response-middleware
     wrap-exceptions
     wrap-reload]
  [#_wrap-context
   ;; query-params & form-params
   #_ parameters/parameters-middleware
   ;; content-negotiation
   muuntaja/format-negotiate-middleware
   ;; encoding response body
   muuntaja/format-response-middleware
   ;; exception handling
   #_exception-middleware
   ;; decoding request body
   muuntaja/format-request-middleware
   ;; coercing response bodys
   #_ coercion/coerce-response-middleware
   ;; coercing request parameters
   #_coercion/coerce-request-middleware
   ;; multipart
   #_multipart/multipart-middleware
   ;; auth
   #_auth
   #_restrict-middleware
   ;; Set `flexblock.modals.helpers/*master*`
   #_ master-middleware
   ;; gzip
   #_gzip/wrap-gzip])
