(ns csld.image
  (:require [mikera.image.core :as i]
            [mikera.image.colours :as c]
            [clojure.java.io :as io]))

(def dbg (slurp (io/resource "dbg.txt")))

(defn load-image [image]
  (->> image
       (str "img/")
       i/load-image-resource))

(def caesar (mapv load-image
                  ["caesar1.jpg"
                   "caesar2.jpg"
                   "caesar5.jpg"
                   "jcaesar-bust-001.jpg"
                   "julius-caesar-s.jpg"
                   "julius-caesar_2571737b.jpg"]))

(defn resize [x y img]
  (i/resize img x y))

(defn ->img [img]
  {:width  (i/width img)
   :height (i/height img)
   :pixels (mapv c/components-rgb (i/get-pixels img))})

(defn char-pixels [str pixels]
  (mapv (fn [c pixel index]
          {:character c
           :rgb       pixel
           :key       index})
        str
        pixels
        (range)))

(defn process-img [width height img txt]
  (->
   (resize width height img)
   ->img
   (update :pixels (partial char-pixels txt))))

(defn text-section [width height text]
  (let [drop-size (- (count text) (* width height))]
    (drop (rand-int drop-size) text)))

(defn default-image [width height]
  (process-img width height
               (rand-nth caesar)
               (text-section width height dbg)))
