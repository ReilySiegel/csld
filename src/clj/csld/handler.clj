(ns csld.handler
  (:require
   [reitit.ring :as reitit-ring]
   [csld.middleware :refer [middleware]]
   [csld.image :as image]
   [hiccup.page :refer [include-js include-css html5]]
   [muuntaja.core :as m]
   [config.core :refer [env]]
   [cheshire.core :as json]))

(def mount-target
  [:div#app
   [:h2 "Welcome to csld"]
   [:p "please wait while Figwheel is waking up ..."]
   [:p "(Check the js console for hints if nothing exciting happens.)"]])

(defn head []
  [:head
   [:meta {:charset "utf-8"}]
   [:meta {:name    "viewport"
           :content "width=device-width, initial-scale=1"}]
   (include-css (if (env :dev) "/css/site.css" "/css/site.min.css"))
   (include-css "https://fonts.googleapis.com/css?family=Fira+Mono")])

(defn loading-page []
  (html5
   (head)
   [:body {:class "body-container"}
    mount-target
    (include-js "/js/app.js")]))

(defn index-handler
  [_request]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    (loading-page)})

(defn image-handler [{{:keys [width height]} :path-params}]
  {:status  200
   :headers {"Content-Type" "application/json"}
   :body    (json/encode (image/default-image (bigint width)
                                              (bigint height)))})

(def app
  (reitit-ring/ring-handler
   (reitit-ring/router
    [["/" {:get {:handler index-handler}}]
     ["/image/{width}x{height}" {:get {:handler image-handler}}]]
    {:middleware middleware})
   (reitit-ring/routes
    (reitit-ring/create-resource-handler {:path "/" :root "/public"})
    (reitit-ring/create-default-handler))))
