(ns csld.core
  (:require
   [reagent.core :as reagent :refer [atom]]
   [ajax.core :as http]
   [clojure.string :as str]))

(def width (atom 1))

(def height (atom 1))

(def text (atom {:pixels {:character \.
                          :key       1
                          :rgb       [0 0 255]}}))

(defn width-of-reference []
  (try (.-width
        (.getBoundingClientRect
         (.getElementById js/document "reference-span")))
       (catch :default e 0)))

(defn height-of-reference []
  (try  (.-height
         (.getBoundingClientRect
          (.getElementById js/document "reference-span")))
        (catch :default e 0)))

(defn width-of-frame []
  (try (.-clientWidth (.-documentElement js/document))
       (catch :default e 0)))

(defn height-of-frame []
  (try (.-clientHeight (.-documentElement js/document))
       (catch :default e 0)))

(defn update-width-height []
  (reset! width (.floor js/Math
                        (/ (width-of-frame)
                           (width-of-reference))))
  (reset! height (.floor js/Math
                         (/ (height-of-frame)
                            (height-of-reference)))))
(defn update-text
  ([] (update-text false))
  ([force]
   (when (or force
             (not (and (= @width (:width @text))
                       (= @height (:height @text)))))
     (http/GET (str "/image/" @width "x" @height)
               {:handler         #(reset! text %)
                :response-format :json
                :keywords?       true}))))

(defn rgb-string [r g b]
  (str "rgb(" (str/join "," [r g b]) ")"))

(defn home-page []
  (reagent/create-class
   {:component-did-update #(update-text)
    :reagent-render
    (fn []
      [:div
       [:span
        {:id    :reference-span
         :style {:position :absolute
                 :top      :-100px}}
        \c]
       [:div
        (take
         @height
         (doall (for [row
                      (partition-all @width (:pixels @text))]
                  [:div {:key (* 10000 (:key (first row)))}
                   (for [{:keys [rgb character key]} row]
                     [:span
                      {:key   key
                       :style {:color (apply rgb-string rgb)}}
                      character])])))]])}))

;; -------------------------
;; Initialize app
(defn mount-root []
  (reagent/render [home-page] (.getElementById js/document "app")))

(defn init! []
  (mount-root)
  (js/setInterval
   (fn []
     (update-width-height)
     (update-text true))
   10000)
  #_(js/setInterval
     (fn []
       (update-text true))
     30000))
